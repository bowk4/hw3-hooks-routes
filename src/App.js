import React, {useState, useEffect} from 'react';
import './App.css';
import {Header} from "./components/Header";
import {Footer} from "./components/Footer";
import {Items} from "./components/Items";
import {Category} from "./components/Category";
import {Link, Route, Router, Routes} from "react-router-dom";
import {Layout} from "./components/Layout";
import {ShopItems} from "./components/ShopItems";
import {SaveItems} from "./components/SaveItems";
import {SavedStar} from "./components/SavedStar";
import {Order} from "./components/Order";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            orders: [],
            savedStar: [],

            currentItems: [],
            savedItems: [],

            items: [],

            showFullItem: false,

            cartCount: 0, // Лічильник товарів у кошику
            savedCount: 0, // Лічильник збережених товарів
        }
        this.state.currentItems = this.state.items
        this.state.savedItems = this.state.items
        this.addToOrder = this.addToOrder.bind(this)
        this.addToSaved = this.addToSaved.bind(this)
        this.deleteOrder = this.deleteOrder.bind(this)
        this.deleteSaved = this.deleteSaved.bind(this);
        this.chooseCategory = this.chooseCategory.bind(this)
    }

    async fetchItems() {
        try {
            const response = await fetch(`/getItem.json`);
            const data = await response.json();

            this.setState({
                items: data.items,
                currentItems: data.items,
                savedItems: data.items,
            });
        } catch (error) {
            console.warn(error);
        }
    }


    async componentDidMount() {
        // Load items from the server on component mount
        await this.fetchItems(); // Wait for fetchItems to complete
        // Load orders from local storage on component mount
        const storedOrders = JSON.parse(localStorage.getItem('orders')) || [];
        const storedStars = JSON.parse(localStorage.getItem('savedStar')) || [];
        this.setState({orders: storedOrders, savedStar: storedStars});
    }

    componentDidUpdate() {
        // Save orders to local storage whenever component state updates
        localStorage.setItem('orders', JSON.stringify(this.state.orders));
        localStorage.setItem('savedStar', JSON.stringify(this.state.savedStar));
    }

    render() {
        return (
            <div className="wrapper">
                <Header orders={this.state.orders} saved={this.state.savedStar} onDeleteOrder={this.deleteOrder}
                        onDeleteSaved={this.deleteSaved}/>
                <Routes>
                    <Route path="/" element={
                        <Layout
                            chooseCategory={this.chooseCategory}
                            currentItems={this.state.currentItems}
                            savedItems={this.state.savedItems}
                            addToOrder={this.addToOrder}
                            addToSaved={this.addToSaved}
                            deleteToSaved={this.deleteSaved}
                        />
                    }/>
                </Routes>

            </div>
        );
    }

    chooseCategory(category) {
        if (category === 'all') {
            this.setState({currentItems: this.state.items, savedItems: this.state.items})
            return
        }
        this.setState({
            currentItems: this.state.items.filter(el => el.category === category),
            savedItems: this.state.items.filter(el => el.category === category)
        })
    }

    addToOrder(item) {
        const existingItem = this.state.orders.find((el) => el.id === item.id);

        if (existingItem) {
            // Якщо товар вже є в кошику, оновіть його кількість
            const updatedOrders = this.state.orders.map((el) =>
                el.id === item.id ? { ...el, quantity: el.quantity + 1 } : el
            );

            this.setState({
                orders: updatedOrders,
                cartCount: this.state.cartCount + 1,
            });
        } else {
            // Якщо товара немає в кошику, додайте його як новий товар
            this.setState({
                orders: [...this.state.orders, { ...item, quantity: 1 }],
                cartCount: this.state.cartCount + 1,
            });
        }
    }

    addToSaved(item) {
        let isInArray = false;
        this.state.savedStar.forEach(el => {
            if (el.id === item.id) isInArray = true;
        });
        if (!isInArray) {
            this.setState({
                savedStar: [...this.state.savedStar, item],
                savedCount: this.state.savedCount + 1, // Збільшуємо лічильник збережених товарів
            });
        }
    }

    deleteOrder(id) {
        this.setState({
            orders: this.state.orders.filter(el => el.id !== id),
            cartCount: this.state.cartCount - 1, // Зменшуємо лічильник товарів у кошику
        });
    }

    deleteSaved(id) {
        this.setState({
            savedStar: this.state.savedStar.filter(el => el.id !== id),
            savedCount: this.state.savedCount - 1, // Зменшуємо лічильник збережених товарів
        });
    }
}

export default App;
