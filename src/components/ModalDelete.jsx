import React from "react";

export const ModalDelete = ({ onConfirm, onClose, title }) => {
    const handleClick = (e) => {
        // Запобігаємо подальшому розповсюдженню події кліка до батьківського елемента (modal-buy)
        e.stopPropagation();
    };

    return (
        <div className="modal-delete" onClick={handleClick}>
            <h2>{title}</h2>
            <section>
                <button onClick={onConfirm}>Так</button>
                <button onClick={onClose}>Відміна</button>
            </section>
        </div>
    );
};
