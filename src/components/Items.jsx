import React, { Component } from "react";
import { Item } from "./Item";

export class Items extends Component {
    render() {
        // Ensure that this.props.items is an array before attempting to map
        const itemsArray = Array.isArray(this.props.items) ? this.props.items : [];
        console.log('Number of items:', itemsArray.length);

        return (
            <main>
                {itemsArray.map((el) => {
                    console.log('Item:', el);
                    return (
                        <Item
                            key={el.id}
                            item={el}
                            onAdd={this.props.onAdd}
                            onAddSaved={this.props.onAddSaved}
                            onDeleteSaved={this.props.onDeleteSaved}

                        />
                    )
                })}
            </main>
        );
    }
}
