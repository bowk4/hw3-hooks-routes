import React, {Component} from "react";
import {FaTrash} from 'react-icons/fa'
import {ModalDelete} from "./ModalDelete";
import {CiStar} from "react-icons/ci";

export class Order extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openDeleteModal: false,
        }
    }

    render() {
        return (
            <div className="item">
                <img src={"./product-img/" + this.props.item.img} alt="items" width="150px" height="150px"/>
                <h2>{this.props.item.title}</h2>
                <b>{this.props.item.price}$</b>
                <b>Колір: {this.props.item.color}</b>

                <span>Кількість: {this.props.item.quantity || 1}</span>
                <FaTrash
                    className="delete-icon"
                    onClick={() => this.setState({openDeleteModal: true})}
                />


                {this.state.openDeleteModal && (
                    <div className="blur-wrapper" onClick={() => this.setState({openDeleteModal: false})}>
                        <ModalDelete
                            title="Видалити товар з кошика?"
                            onClose={() => this.setState({openDeleteModal: false})}
                            onConfirm={() => {
                                this.props.onDelete(this.props.item.id);
                                this.setState({openDeleteModal: false});
                            }}

                        />
                    </div>
                )}
            </div>
        )
    }
}

// this.props.onDelete(this.props.item.id)}