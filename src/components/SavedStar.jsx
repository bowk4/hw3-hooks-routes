import React, {Component} from "react";
import {FaTrash} from "react-icons/fa";
import {ModalDelete} from "./ModalDelete";

export class SavedStar  extends Component {
    constructor(props) {
        super(props);
        // this.state = {
        //     openDeleteModal: false,
        // }
    }
    render() {
        return (
            <div className="item">
                <img src={"./product-img/" + this.props.item.img} alt="items" width="150px" height="150px"/>
                <h2>{this.props.item.title}</h2>
                <b>{this.props.item.price}$</b>
                {/*<FaTrash className="delete-icon"*/}
                {/*         onClick={() => this.setState({openDeleteModal: true})}*/}
                {/*/>*/}

                {/*{this.state.openDeleteModal && (*/}
                {/*    <div className="blur-wrapper" onClick={() => this.setState({openDeleteModal: false})}>*/}
                {/*        <ModalDelete*/}
                {/*            title="Видалити товар зі збереженого?"*/}
                {/*            onClose={() => this.setState({openDeleteModal: false})}*/}
                {/*            onConfirm={() => {*/}
                {/*                this.props.onDelete(this.props.item.id);*/}
                {/*                this.setState({openDeleteModal: false});*/}
                {/*            }}*/}

                {/*        />*/}
                {/*    </div>*/}
                {/*)}*/}
            </div>
        )
    }
}
