import React, { Component } from "react";
import { CiStar } from "react-icons/ci";
import { ModalBuy } from "./ModalBuy";

export class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openModalBuy: false,
            addedToCart: false,
            isSaved: false, // Додайте стан для відстеження того, чи товар збережено
        };
    }

    componentDidMount() {
        // При завантаженні компонента перевірте localStorage і оновіть стан відповідно
        const savedItems = JSON.parse(localStorage.getItem("savedItems")) || [];
        const { item } = this.props;

        const isSaved = savedItems.some((savedItem) => savedItem.id === item.id);

        // Перевірте, чи товар був доданий до кошика, і чи він ще не був видалений
        if (isSaved) {
            this.setState({ isSaved });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // При зміні стану isSaved збережіть дані в localStorage
        if (prevState.isSaved !== this.state.isSaved) {
            const savedItems = JSON.parse(localStorage.getItem("savedItems")) || [];
            const { item } = this.props;

            if (this.state.isSaved) {
                // Додайте товар до списку збережених, тільки якщо його там ще немає
                const isItemSaved = savedItems.some((savedItem) => savedItem.id === item.id);
                if (!isItemSaved) {
                    savedItems.push(item);
                }
            } else {
                // Видаліть товар із списку збережених
                const index = savedItems.findIndex((savedItem) => savedItem.id === item.id);
                if (index !== -1) {
                    savedItems.splice(index, 1);
                }
            }

            // Оновіть localStorage
            localStorage.setItem("savedItems", JSON.stringify(savedItems));
        }
    }

    toggleSaved = () => {
        const { isSaved } = this.state;
        const { onAddSaved, onDeleteSaved, item } = this.props;

        if (isSaved) {
            // Якщо товар вже збережено, видаліть його зі збереженого
            this.setState({ isSaved: false });
            // Видаліть товар із списку збережених
            this.props.onDeleteSaved(item.id);
        } else {
            // Інакше, додайте товар до списку збережених
            this.setState({ isSaved: true });
            this.props.onAddSaved(item);
        }
    };

    render() {
        const { isSaved, openModalBuy } = this.state;

        return (
            <div className="item">
                <img
                    src={"./product-img/" + this.props.item.img}
                    alt="items"
                    width="260px"
                    height="150px"
                />
                <h2>{this.props.item.title}</h2>
                <p>{this.props.item.desc}</p>
                <p>Код товару: {this.props.item.article}</p>
                <p>Колір: {this.props.item.color}</p>
                <b>{this.props.item.price}$</b>
                <section className="add-wrapper">
                    <div
                        className={`add_card`}
                        onClick={() => this.setState({ openModalBuy: true })}
                    >
                        +
                    </div>
                    <div className="add_favorite-star">
                        <CiStar
                            className={`star ${isSaved ? "saved" : ""}`}
                            onClick={this.toggleSaved}
                        />
                    </div>
                </section>

                {openModalBuy && (
                    <div
                        className="blur-wrapper"
                        onClick={() => this.setState({ openModalBuy: false })}
                    >
                        <ModalBuy
                            title="Ви впевнені, що хочете додати даний товар в кошик?"
                            onClose={() => this.setState({ openModalBuy: false })}
                            onConfirm={() => {
                                this.props.onAdd(this.props.item);
                                this.setState({ openModalBuy: false, addedToCart: true });
                            }}
                        />
                    </div>
                )}
            </div>
        );
    }
}
