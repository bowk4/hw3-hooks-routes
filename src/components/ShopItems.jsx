import {FaTrash} from "react-icons/fa";
import React, {Component} from "react";

export class ShopItems extends Component {
    render() {
    return (
        <main className="item">
            <img src={"./product-img/" + this.props.item.img} alt="items" width="150px" height="150px"/>
            <h2>{this.props.item.title}</h2>
            <b>{this.props.item.price}$</b>
            <b>Колір: {this.props.item.color}</b>
            <FaTrash className="delete-icon" onClick={() => this.props.onDelete(this.props.item.id)}/>
        </main>
    )
    }
}