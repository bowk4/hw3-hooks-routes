import React from "react";
import { AiOutlineCloseCircle } from "react-icons/ai";

export const ModalBuy = ({ onConfirm, onClose, title }) => {
    const handleClick = (e) => {
        // Запобігаємо подальшому розповсюдженню події кліка до батьківського елемента (modal-buy)
        e.stopPropagation();
    };

    return (
        <div className="modal-buy" onClick={handleClick}>
            <AiOutlineCloseCircle className="close-modal" onClick={onClose}/>
            <h2>{title}</h2>
            <section>
                <button onClick={onConfirm}>Так</button>
                <button onClick={onClose}>Відміна</button>
            </section>
        </div>
    );
};
