import React, {useState} from "react";
import {FaShoppingCart} from "react-icons/fa";
import {Order} from "./Order";
import {CiStar} from "react-icons/ci";
import {SavedStar} from "./SavedStar";
import {Link, Route, Routes} from "react-router-dom";
import {Layout} from "./Layout";
import {SaveItems} from "./SaveItems";
import {Footer} from "./Footer";
import {ShopItems} from "./ShopItems";

// const sumOrders = (props) => {
//     let summa = 0
//     props.orders.forEach(el => summa += Number.parseFloat(el.price))
//         return (
//             <>
//                 <p className="summa">Сума: {new Intl.NumberFormat().format(summa)}$</p>
//             </>
//         )
// }

const showOrders = (props) => {
    // let summa = 0
    // props.orders.forEach(el => summa += Number.parseFloat(el.price))
    return (
        <>
            {props.orders.map(el => (
                <Order onDelete={props.onDeleteOrder} key={el.id} item={el}/>
            ))}
            {/*<p className="summa">Сума: {new Intl.NumberFormat().format(summa)}$</p>*/}
        </>
    )
}

const showSaved = (props) => {
    return (
        <>
            {props.saved.map(el => (
                <SavedStar onDelete={props.onDeleteSaved} key={el.id} item={el}/>
            ))}
        </>
    )
}

const showNothing = () => {
    return (
        <div className="empty">
            <h2>Кошик пустий</h2>
        </div>
    )
}
const showNothingSave = () => {
    return (
        <div className="empty">
            <h2>Збережень немає</h2>
        </div>
    )
}

export const Header = (props) => {
    let [cartOpen, setCartOpen] = useState(false)
    let [saveOpen, setSaveOpen] = useState(false)

    // Підрахунок кількості товарів у кошику
    const cartItemCount = props.orders.length;

    // Підрахунок кількості збережених товарів
    const savedItemCount = props.saved.length;

    return (
        <header>
            <div>
                <Link
                    to="/" className="logo">
                    <img className="logo-img" src="./logo-img/vital_logo_small.png" alt="logo"/>
                </Link>
                <ul className="nav">
                    {/*<li>*/}
                    {/*    /!*<span className="item-count-save">{savedItemCount}</span>{" "}*!/*/}
                    {/*    /!* Відображення кількості збережених товарів *!/*/}
                    {/*    <CiStar*/}
                    {/*        onClick={() => setSaveOpen((saveOpen) => !saveOpen)}*/}
                    {/*        className={`save-card-btn ${saveOpen && "active"}`}*/}
                    {/*    />*/}
                    {/*    {saveOpen && (*/}
                    {/*        <div className="save-cart">*/}
                    {/*            {props.saved.length > 0 ? (*/}
                    {/*                showSaved(props)*/}
                    {/*            ) : (*/}
                    {/*                showNothingSave()*/}
                    {/*            )}*/}
                    {/*        </div>*/}
                    {/*    )}*/}
                    {/*</li>*/}
                    {/*<li>*/}
                    {/*    <FaShoppingCart*/}
                    {/*        onClick={() => setCartOpen((cartOpen) => !cartOpen)}*/}
                    {/*        className={`shop-card-btn ${cartOpen && "active"}`}*/}
                    {/*    />*/}
                    {/*    /!*<span className="item-count-shop">{cartItemCount}</span>{" "}*!/*/}
                    {/*    /!* Відображення кількості товарів у кошику *!/*/}
                    {/*    {cartOpen && (*/}
                    {/*        <div className="shop-cart">*/}
                    {/*            {props.orders.length > 0 ? (*/}
                    {/*                showOrders(props)*/}
                    {/*            ) : (*/}
                    {/*                showNothing()*/}
                    {/*            )}*/}
                    {/*        </div>*/}
                    {/*    )}*/}
                    {/*</li>*/}
                    <li>
                        <Link to="/">На головну</Link>
                    </li>
                    <li>
                        <section>
                            <span className="item-count-shop">{cartItemCount}</span>{" "}
                            <Link to="/shop">Кошик товарів</Link>
                        </section>
                    </li>
                    <li>
                        <section>
                            <span className="item-count-save">{savedItemCount}</span>{" "}
                            <Link to="/save">Збережене</Link>
                        </section>
                    </li>
                </ul>

                <Routes>
                    {/*<Route path="/" element={<Footer />}/>*/}
                    <Route
                        path="/save"
                        element={
                            <>
                                <h2 className="route_title">Мої збереження</h2>
                                <div className="main_route-card">
                                    {props.saved.length > 0 ? (
                                        showSaved(props)
                                    ) : (
                                        showNothingSave()
                                    )}
                                </div>
                            </>
                        }
                    />
                    <Route
                        path="/shop"
                        element={
                            <>
                                <h2 className="route_title">Мій кошик</h2>
                                <div className="main_route-card">
                                    {props.orders.length > 0 ? (
                                        showOrders(props)
                                    ) : (
                                        showNothing()
                                    )}
                                </div>
                            </>
                        }
                    />
                </Routes>
            </div>
            {/*<div className="presentation"></div>*/}
        </header>
    )
}
