
import React from "react";
import {Footer} from "./Footer";
import {Outlet} from "react-router-dom";
import {Category} from "./Category";
import {Items} from "./Items";

export const Layout = (props) => {
    return (
        <>
            <Category chooseCategory={props.chooseCategory} />
            <Items
                items={props.currentItems && props.savedItems}
                onAdd={props.addToOrder}
                onAddSaved={props.addToSaved}
                onDeleteSaved={props.deleteToSaved}
            />
            <Footer />
        </>
    );
};