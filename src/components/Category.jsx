import React, {Component} from "react";

export class Category  extends Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: [
                {
                    key: 'all',
                    name: 'Все'
                },
                {
                    key: 'chairs',
                    name: 'Стільці'
                },
                {
                    key: 'tables',
                    name: 'Столи'
                },
                {
                    key: 'sofas',
                    name: 'Дивани'
                },
                {
                    key: 'flashlights',
                    name: 'Фонарі'
                }
            ]
        }
    }
    render() {
        return (
            <div className="categories">
                {this.state.categories.map(el => (
                    <div key={el.key} onClick={() => this.props.chooseCategory(el.key)}>{el.name}</div>
                ))}
            </div>
        )
    }
}
