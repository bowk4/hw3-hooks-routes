import {FaTrash} from "react-icons/fa";
import React from "react";

export const SaveItems = () => {
    return (
        <div className="item">
            <p>Збережене</p>
            <img src={"./product-img/" + this.props.item.img} alt="items" width="150px" height="150px"/>
            <h2>{this.props.item.title}</h2>
            <b>{this.props.item.price}$</b>
            <b>Колір: {this.props.item.color}</b>
            <FaTrash className="delete-icon" onClick={() => this.props.onDelete(this.props.item.id)}/>
        </div>
    )
}